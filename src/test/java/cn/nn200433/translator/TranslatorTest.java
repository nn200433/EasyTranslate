package cn.nn200433.translator;

import cn.hutool.core.lang.Console;
import cn.nn200433.translator.annotation.Translator;
import cn.nn200433.translator.demo.DemoService;
import cn.nn200433.translator.entity.Device;
import cn.nn200433.translator.entity.People;
import cn.nn200433.translator.entity.People2;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * 转换测试
 *
 * @author nn200433
 * @date 2020年05月18日 0018 15:05:05
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class TranslatorTest {
    
    @Autowired
    private DemoService demoService;
    
    @Test
    public void demo1() {
        List<People> peopleList = demoService.dictDemo();
        Console.log("---> 翻译结果：{}", peopleList);
    }
    
    @Test
    public void demo2() {
        List<Device> deviceList = demoService.enumDemo();
        Console.log("---> 翻译结果：{}", deviceList);
    }
    
    @Test
    public void demo3() {
        List<People2> peopleList = demoService.dbDemo();
        Console.log("---> 翻译结果：{}", peopleList);
    }
    
}
