package cn.nn200433.translator.service.impl;

import cn.nn200433.translator.service.DictTranslateService;

/**
 * 默认字典翻译服务实现
 *
 * @author nn200433
 * @date 2022-08-18 018 16:32:24
 */
public class DefaultDictTranslateServiceImpl implements DictTranslateService {

    @Override
    public String findDictLabel(String dictCode, String dictValue) {
        return dictValue;
    }

}
