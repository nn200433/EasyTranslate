package cn.nn200433.translator.demo;

import cn.nn200433.translator.entity.Device;
import cn.nn200433.translator.entity.People;
import cn.nn200433.translator.entity.People2;

import java.util.List;

/**
 * 演示服务
 *
 * @author nn200433
 * @date 2022-12-16 016 11:45:46
 */
public interface DemoService {
    
    /**
     * 字典演示
     *
     * @return {@link List }<{@link People }>
     * @author nn200433
     */
    public List<People> dictDemo();
    
    /**
     * 枚举演示
     *
     * @return {@link List }<{@link Device }>
     * @author nn200433
     */
    public List<Device> enumDemo();
    
    /**
     * 数据库演示
     *
     * @return {@link List }<{@link People2 }>
     * @author nn200433
     */
    public List<People2> dbDemo();
    
}
