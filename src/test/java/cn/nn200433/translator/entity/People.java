package cn.nn200433.translator.entity;

import cn.nn200433.translator.annotation.Dictionary;
import cn.nn200433.translator.annotation.Translate;
import cn.nn200433.translator.constants.DesensitizedTypeConstants;
import cn.nn200433.translator.demo.impl.CustomerTranslateServiceImpl;
import cn.nn200433.translator.desensitized.Desensitized;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 人
 *
 * @author nn200433
 * @date 2022-12-16 016 11:40:30
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class People {
    
    @Translate(dictClass = Dict.class, groupValue = "sex", translateField = "sexName")
    private String sex;
    
    private String sexName;
    
    /** 手机号脱敏 */
    @Translate(dictClass = Desensitized.class, translateField = "phone", desensitizedModel = DesensitizedTypeConstants.MOBILE_PHONE)
    private String phone;
    
    /** 自定义翻译 */
    @Translate(dictionary = @Dictionary(translator = CustomerTranslateServiceImpl.class), translateField = "name")
    private String id;
    
    private String name;
    
}
