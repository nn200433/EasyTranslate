package cn.nn200433.translator.demo.impl;

import cn.hutool.core.collection.CollUtil;
import cn.nn200433.translator.annotation.Translator;
import cn.nn200433.translator.demo.DemoService;
import cn.nn200433.translator.entity.Device;
import cn.nn200433.translator.entity.People;
import cn.nn200433.translator.entity.People2;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 演示服务实现
 *
 * @author nn200433
 * @date 2022-12-16 016 11:46:57
 */
@Component
public class DemoServiceImpl implements DemoService {
    
    @Translator
    @Override
    public List<People> dictDemo() {
        People man   = People.builder().sex("1").id("1").phone("18612345678").build();
        People woman = People.builder().sex("2").id("2").phone("18612345678").build();
        return CollUtil.newArrayList(man, woman);
    }
    
    @Translator
    @Override
    public List<Device> enumDemo() {
        Device d1   = Device.builder().status("1").build();
        Device d2 = Device.builder().status("2").build();
        return CollUtil.newArrayList(d1, d2);
    }
    
    @Translator
    @Override
    public List<People2> dbDemo() {
        People2 man   = People2.builder().id("1").build();
        People2 woman = People2.builder().id("2").build();
        return CollUtil.newArrayList(man, woman);
    }
    
}
